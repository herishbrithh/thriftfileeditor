# -*- coding: utf-8 -*-

import thriftpy

import logging
logging.basicConfig()

from thriftpy.protocol import TCyBinaryProtocolFactory
from thriftpy.transport import TCyBufferedTransportFactory
from thriftpy.rpc import make_server

edit_thrift = thriftpy.load("edit.thrift", module_name="edit_thrift")

class Dispatcher(object):

	def edit(self):

		lines = open("a.txt").readlines()
		open("a.txt", "w").writelines(lines[:-10])
		return "done"

def main():

	serverA = make_server(edit_thrift.Editor, Dispatcher(), '127.0.0.1', 6000, proto_factory=TCyBinaryProtocolFactory(), trans_factory=TCyBufferedTransportFactory())
	print("serving...")
	serverA.serve()

if __name__ == '__main__':
	main()
