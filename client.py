# -*- coding: utf-8 -*-

import thriftpy

from thriftpy.protocol import TCyBinaryProtocolFactory
from thriftpy.transport import TCyBufferedTransportFactory
from thriftpy.rpc import client_context

edit_thrift = thriftpy.load("edit.thrift", module_name="edit_thrift")


def main():

	with client_context(edit_thrift.Editor, '127.0.0.1', 6000, proto_factory=TCyBinaryProtocolFactory(), trans_factory=TCyBufferedTransportFactory()) as ed:

		f = ed.edit()
		print "a.txt edited"

	with client_context(edit_thrift.Editor, '127.0.0.1', 7000, proto_factory=TCyBinaryProtocolFactory(), trans_factory=TCyBufferedTransportFactory()) as ed:

		f = ed.edit()
		print "b.txt edited"


if __name__ == '__main__':
	main()
